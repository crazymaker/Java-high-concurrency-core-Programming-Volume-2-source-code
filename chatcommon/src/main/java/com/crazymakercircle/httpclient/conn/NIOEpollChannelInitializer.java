package com.crazymakercircle.httpclient.conn;

import com.crazymakercircle.httpclient.NettyConfig;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpResponseDecoder;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @ClassName NIOChannelInitializer
 * @Author 40岁老架构师 尼恩 @ 公众号 技术自由圈
 */

public class NIOEpollChannelInitializer extends ChannelInitializer<EpollSocketChannel> {

    NettyConfig conf;
    NettyHttpHandler.Listener listener;
    public NIOEpollChannelInitializer(NettyConfig conf,NettyHttpHandler.Listener listener) {
        this.conf = conf;
        this.listener = listener;
    }

    @Override
    protected void initChannel(EpollSocketChannel ch) throws Exception {
        ch.pipeline().addLast("idle-handler", new IdleStateHandler(0,
                0,
                conf.getIdleHeartbeatInterval()));
        ch.pipeline().addLast("http-codec", new HttpResponseDecoder());
        ch.pipeline().addLast("response-body-cumulate", new HttpObjectAggregator(conf.getMaxRespBodyLen(),
                true));
        ch.pipeline().addLast("http-pipeline-handler", new NettyHttpHandler(listener));
    }
}
