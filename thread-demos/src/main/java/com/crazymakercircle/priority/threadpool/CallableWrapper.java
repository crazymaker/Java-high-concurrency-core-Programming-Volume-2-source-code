package com.crazymakercircle.priority.threadpool;

import com.crazymakercircle.util.Print;

import java.util.concurrent.Callable;

public class CallableWrapper implements Callable<String> {
    private final Integer taskId;

    public CallableWrapper(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getTaskId() {
        return this.taskId;
    }

    @Override
    public String call() {
        Print.tcfo("Task " + taskId + " is running.");
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
            // ignore
        }
        return "Task " + taskId + " is completed.";
    }
}
