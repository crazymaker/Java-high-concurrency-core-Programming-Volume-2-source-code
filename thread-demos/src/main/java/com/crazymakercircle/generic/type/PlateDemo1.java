package com.crazymakercircle.generic.type;

import com.crazymakercircle.mutithread.basic.threadlocal.Foo;
import org.junit.Test;

//食物
class Food {
}

//水果、肉 都是食物
class Fruit extends Food {
}

class Meat extends Food {
}

// 苹果 香蕉 都是水果
class Apple extends Fruit {
}

class Banana extends Fruit {
}

//猪肉、 牛肉 都是肉
class Pork extends Meat {
}

class Beef extends Meat {
}

//山西苹果、云南苹果 都是苹果
class 山西Apple extends Apple {
}

class 云南Apple extends Apple {
}


//盘子，可以装 任何东西，包括 食物 其他
class PlateDemo1<T> {

    //盘子里的东西
    private T someThing;

    public PlateDemo1(T t) {
        someThing = t;
    }

    public PlateDemo1() {
    }

    public void set(T t) {
        someThing = t;
    }

    public T get() {
        return someThing;
    }


    public static void main(String[] args) {

        //创建一个装肉的盘子
        PlateDemo1<Meat> plateDemo1 = new PlateDemo1<>(new Pork());

        //创建一个装水果的盘子
        PlateDemo1<Fruit> plateDemo2 = new PlateDemo1<>(new Apple());

    }



}

